import Weather from "js/components/Weather";

class App extends Component {
  buttonClick() {
    this.props.toggleButton();
  }

  render() {
    const { testState } = this.props;
    let testmsg;

    if (testState) {
      testmsg = <div>Hide settings</div>;
    } else {
      testmsg = <div>Show settings</div>;
    }

    return (
      <Fragment>
        <header className="header flex-grow-0 flex-shrink-0 w-100 py-5">
          <div className="container-fluid">
            <button
              type="button"
              className={classNames("button", { active: testState })}
              onClick={() => this.buttonClick()}
            >
              {testmsg}
            </button>
          </div>
        </header>
        <main className="main flex-grow-1 flex-shrink-0">
          <div className="container-fluid">
            <Weather />
          </div>
        </main>
        <footer className="footer flex-grow-0 flex-shrink-0 w-100">
          <div className="container-fluid">footer</div>
        </footer>
      </Fragment>
    );
  }
}

export default connect(
  (state) => ({
    testState: state.togglePopup.showMenu
  }),
  (dispatch) => ({
    toggleButton: () => {
      dispatch({ type: "TOGGLE_POPUP" });
    }
  })
)(App);
