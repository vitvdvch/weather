import { call, put, takeEvery, delay } from "redux-saga/effects";

export function* watchWeather() {
  yield takeEvery("GET_DATA", getWeatherAsync);
}

function fetchData(city, unit) {
  return axios({
    method: "get",
    url: "http://api.openweathermap.org/data/2.5/weather",
    params: {
      q: city,
      units: unit,
      APPID: "19913af45d9c75fcd761776198b2e090"
    }
  });
}

function* getWeatherAsync(action) {
  try {
    yield put({ type: "LOADING", preloader: true });
    yield delay(2000); // like settimeout
    const data = yield call(fetchData, action.city, action.unit);
    yield put({ type: "SET_WEATHER", forecast: data.data });
  } catch (error) {
    yield put({ type: "LOADING", preloader: false });
    yield put({ type: "REQUEST_ERROR" });
    console.log("getWeatherAsync: " + error);
  }
}
