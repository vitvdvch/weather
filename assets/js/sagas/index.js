import { all } from "redux-saga/effects";
import { watchWeather } from "js/sagas/weather";

export default function* rootSaga() {
  yield all([
    watchWeather()
    // add other watchers to the array
  ]);
}
