import togglePopup from "js/reducers/TogglePopup";
import Weather from "js/reducers/Weather";

const storeApp = combineReducers({
  togglePopup,
  Weather
});

export default storeApp;
