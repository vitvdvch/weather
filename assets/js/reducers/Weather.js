const initialState = {
  cities: [
    {
      name: "London",
      value: "London,uk"
    },
    {
      name: "Stockholm",
      value: "Stockholm"
    },
    {
      name: "Kyiv",
      value: "Kyiv"
    }
  ],
  currentCity: "London,uk",
  units: [
    {
      name: "Kelvin",
      value: "kelvin"
    },
    {
      name: "Fahrenheit",
      value: "imperial"
    },
    {
      name: "Celsius",
      value: "metric"
    }
  ],
  currentUnit: "metric",
  weather: {
    main: {
      humidity: "",
      pressure: "",
      temp: "",
      temp_max: "",
      temp_min: ""
    },
    wind: {
      deg: "",
      speed: ""
    },
    clouds: {
      all: ""
    }
  },
  preloader: false,
  error: false
};

const Weather = (state = initialState, action) => {
  switch (action.type) {
    case "SELECT_CITY":
      return { ...state, currentCity: action.newCity };
    case "SELECT_UNIT":
      return { ...state, currentUnit: action.newUnit };
    case "LOADING":
      return { ...state, preloader: action.preloader };
    case "REQUEST_ERROR":
      return {
        ...state,
        error: true
      };
    case "SET_WEATHER":
      return {
        ...state,
        preloader: false,
        error: false,
        weather: {
          ...state.weather,
          main: {
            ...state.weather.main,
            temp: action.forecast.main.temp,
            temp_max: action.forecast.main.temp_max,
            temp_min: action.forecast.main.temp_min,
            humidity: action.forecast.main.humidity,
            pressure: action.forecast.main.pressure
          },
          wind: {
            ...state.weather.wind,
            deg: action.forecast.wind.deg,
            speed: action.forecast.wind.speed
          },
          clouds: {
            ...state.weather.clouds,
            all: action.forecast.clouds.all
          }
        }
      };
    default:
      return state;
  }
};

export default Weather;
