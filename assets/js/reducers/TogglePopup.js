const initialState = {
  showMenu: true
};

function toggleMenu(state) {
  var newState = Object.assign({}, state);
  newState.showMenu = !newState.showMenu;
  return newState;
}

const togglePopup = (state = initialState, action) => {
  switch (action.type) {
    case "TOGGLE_POPUP":
      return toggleMenu(state);
  }
  return state;
};

export default togglePopup;
