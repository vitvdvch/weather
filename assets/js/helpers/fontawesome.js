import { library, dom, icon } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

import { faFacebookF } from "fontawesome_brands/faFacebookF";
import { faInstagram } from "fontawesome_brands/faInstagram";
import { faTwitter } from "fontawesome_brands/faTwitter";

import { faHeart } from "fontawesome_solid/faHeart";
import { faHeartBroken } from "fontawesome_solid/faHeartBroken";

library.add(faFacebookF, faInstagram, faTwitter, faHeart, faHeartBroken);

Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.config.productionTip = false;
dom.i2svg();
