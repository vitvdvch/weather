import App from "js/App";

import createSagaMiddleware from "redux-saga";
const sagaMiddleware = createSagaMiddleware();
import rootSaga from "js/sagas";

import storeApp from "js/reducers/index";
const store = createStore(storeApp, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);

const renderIfExist = (Component, node) => {
  if (node) {
    ReactDOM.render(Component, node);
  }
};

renderIfExist(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("app")
);
