class Weather extends Component {
  selectCity(event) {
    this.props.setCity(event.target.value);
  }

  selectUnit(event) {
    this.props.setUnit(event.target.value);
  }

  getWeatherForecast(city, unit) {
    this.props.getData(city, unit);
  }

  render() {
    const { settings, cities, currentCity, units, currentUnit, weather, preloader, error } = this.props;

    return (
      <div className="weather">
        {settings && (
          <div className="weather-selectors d-flex align-items-end">
            <div className="weather-selectors--item mr-4">
              <div>City:</div>
              <select defaultValue={currentCity} onChange={(event) => this.selectCity(event)}>
                {cities.map((item, index) => (
                  <option key={index} value={item.value}>
                    {item.name}
                  </option>
                ))}
              </select>
            </div>
            <div className="weather-selectors--item mr-4">
              <div>Units:</div>
              <select defaultValue={currentUnit} onChange={(event) => this.selectUnit(event)}>
                {units.map((item, index) => (
                  <option key={index} value={item.value}>
                    {item.name}
                  </option>
                ))}
              </select>
            </div>
            <div className="weather-selectors--item mr-4">
              <button
                type="button"
                className="button"
                onClick={() => this.getWeatherForecast(currentCity, currentUnit)}
              >
                Change
              </button>
            </div>
          </div>
        )}
        <div className="weather-results">
          {preloader && <div className="my-6">Please, wait.......</div>}
          {!preloader && !error && (
            <ul className="mt-6">
              <li>City: {currentCity}</li>
              <li>Unit: {currentUnit}</li>
              <li>temp: {weather.main.temp}</li>
              <li>temp_max: {weather.main.temp_max}</li>
              <li>temp_min: {weather.main.temp_min}</li>
              <li>humidity: {weather.main.humidity}</li>
              <li>pressure: {weather.main.pressure}</li>
              <li>wind deg: {weather.wind.deg}</li>
              <li>wind speed: {weather.wind.speed}</li>
              <li>clouds: {weather.clouds.all}</li>
            </ul>
          )}
        </div>
        {error && <div className="weather-error my-6">Sorry, error. I can not get the data :(</div>}
      </div>
    );
  }
}

export default connect(
  (state) => ({
    settings: state.togglePopup.showMenu,
    cities: state.Weather.cities,
    currentCity: state.Weather.currentCity,
    units: state.Weather.units,
    currentUnit: state.Weather.currentUnit,
    weather: state.Weather.weather,
    error: state.Weather.error,
    preloader: state.Weather.preloader
  }),
  (dispatch) => ({
    setCity: (value) => {
      dispatch({ type: "SELECT_CITY", newCity: value });
    },
    setUnit: (value) => {
      dispatch({ type: "SELECT_UNIT", newUnit: value });
    },
    getData: (city, unit) => {
      dispatch({ type: "GET_DATA", city, unit });
    }
  })
)(Weather);
